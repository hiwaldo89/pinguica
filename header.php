<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="e1828d1d-7cf5-4f0d-a412-43a8045ef948";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>
<body <?php body_class(); ?>>
    <header class="pinguica-header">
        <div class="container">
            <div class="brand">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/logo.svg" alt="Pingüica diseño floral">
            </div>
            <?php wp_nav_menu(array(
                'menu' => 'menu-1',
                'container' => false
            )); ?>
        </div>
    </header>