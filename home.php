<?php get_header(); ?>
<div class="container py-5">
    <div class="row py-5">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
            <?php $gallery = get_field('galeria'); ?>
            <div class="col-md-6 col-lg-4 col-xl-3 mb-4">
                <a href="<?php echo $gallery[0]['url']; ?>" class="d-block text-center" data-lightbox="gallery-<?php the_ID(); ?>">
                    <div class="gallery-item bg-cover mb-4" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                    <h3 class="marker-purple"><?php the_title(); ?></h3>
                </a>
                <?php foreach($gallery as $key => $image) : ?>
                    <?php if ($key !== 0) : ?>
                        <a href="<?php echo $image['url']; ?>" data-lightbox="gallery-<?php the_ID(); ?>" class="d-none"></a>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php endwhile; endif ?>
    </div>
</div>
<?php get_footer(); ?>