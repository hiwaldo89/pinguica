( function( $ ) {

	// Paroller js
	$( '.paroller' ).paroller();

	function formatNumber( num ) {
		return num.toString().replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' );
	}

	// $( '.service-item' ).click( function( e ) {
	// 	var description = $( this ).data( 'description' );
	// 	var price = $( this ).data( 'price' );
	// 	var image = $( this ).data( 'img' );
	// 	console.log( image );
	// 	var descriptionContainer = $( '.service-modal__description' );
	// 	var priceContainer = $( '.service-modal__price' );
	// 	var imageContainer = $( '.service-modal__img' );
	// 	e.preventDefault();
	// 	descriptionContainer.html( description );
	// 	priceContainer.html( formatNumber( price ) );
	// 	imageContainer.css( 'background-image', `url(${image})` );
	// 	$( '#service-modal' ).modal( 'show' );
	// });
	$( '.display-service-modal' ).click( function() {
		let modalId = $( this ).attr( 'href' );
		$( modalId ).modal( 'show' );

		// $(modalId).on("shown.bs.modal", function() {
		// 	$(modalId + " .service-slider").slick({
		// 		arrows: false,
		// 		fade: true,
		// 		autoplay: true,
		// 		speed: 200,
		// 		pauseOnFocus: false,
		// 		pauseOnHover: false
		// 	});
		// });
	});

	$( '.service-modal' ).on( 'hidden.bs.modal', function( e ) {
		let modalId = $( e.target ).get( 0 ).id;

		$( '#' + modalId + ' .service-slider' ).slick( 'unslick' );
	});

	$( '.service-modal' ).on( 'shown.bs.modal', function( e ) {
		let modalId = $( e.target ).get( 0 ).id;
		$( '#' + modalId + ' .service-slider' ).slick({
			arrows: false,
			fade: true,
			autoplay: true,
			speed: 150,
			pauseOnFocus: false,
			pauseOnHover: false
		});
	});
}(jQuery));
