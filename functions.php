<?php

function pinguica_setup() {
    // Make theme available for translation.
    load_theme_textdomain( 'pinguica', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );
    
    // Let WordPress manage the document title.
    add_theme_support( 'title-tag' );

    // Enable support for Post Thumbnails on posts and pages.
    add_theme_support( 'post-thumbnails' );

    // Register menus
    register_nav_menus( array(
        'menu-1' => esc_html__( 'Primary', 'pinguica' ),
    ) );

    // Switch default core markup for search form, comment form, and comments to output valid HTML5.
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );
}
add_action( 'after_setup_theme', 'pinguica_setup' );

// Enqueue scripts and styles.
function pinguica_scripts() {
    wp_enqueue_style( 'bootstrap-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' );
    wp_enqueue_style( 'slick-carousel-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Raleway&display=swap' );
    wp_enqueue_style( 'pinguica-style', get_stylesheet_uri(), '', time() );

    wp_enqueue_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'fontawesome', 'https://pro.fontawesome.com/releases/v5.7.2/js/all.js', '', '', true );
    wp_enqueue_script( 'lightbox-js', get_template_directory_uri() . '/assets/js/lightbox.js', '', '', true );
    wp_enqueue_script( 'slick-carousel', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/assets/js/vendor.min.js', '', time(), true );
    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/custom.min.js', array('jquery'), time(), true );
}
add_action( 'wp_enqueue_scripts', 'pinguica_scripts' );

// GravityForms compatibility file
require_once get_template_directory() . '/inc/pinguica-gravityforms.php';