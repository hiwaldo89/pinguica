<?php
/**
 * Template Name: Contact
 */
get_header(); ?>
    <div class="p-contact bg-pink">
        <div class="container py-5">
            <div class="row py-5">
                <div class="col-md-10 col-lg-7 mx-auto pt-5 mt-5">
                    <h2 class="text-center h3 mb-5">¿Tienes un evento? ¿Quieres mayores informes?</h2>
                    <p class="text-center mb-5">Nos encanta escuchar tus ideas y proponerte conceptos increíbles.</p>
                    <p class="text-center">Platícanos qué tienes en mente.</p>
                    <?php gravity_form(1, false, false, false, false); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>