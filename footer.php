    <footer class="pinguica-footer bg-pink py-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-2 d-flex">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/logo-alt.svg" alt="" class="footer-logo">
                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="col-3 border-footer ml-auto">
                            <h5>Pingüica, <?php echo date('Y'); ?>.</h5>
                            <p>Made with love by <a href="https://somosbloom.com">Bloom</a></p>
                            <div class="row">
                                <div class="col-6">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/bodas.svg" alt="Bodas.com.mx" class="img-fluid">
                                </div>
                                <div class="col-6">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/zankyou.svg" alt="Zankyou" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-3 text-center border-footer">
                            <h5>Síguenos:</h5>
                            <a href="#" target="_blank">Instagram</a> <br>
                            <a href="#" target="_blank">Facebook</a>
                        </div>
                        <div class="col-3 text-center border-footer">
                            <h5>Contáctanos:</h5>
                            <a href="#">info@pinguicafloral.com</a> <br>
                            <a href="#">461.180.8554</a>
                        </div>
                        <!-- <div class="col-3">
                            <h5 class="text-center">Visítanos:</h5>
                            <p>
                                Av. Industrialización 12, int. A 202 <br> 
                                Álamos, 2da sección <br>
                                Querétaro.
                            </p>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>