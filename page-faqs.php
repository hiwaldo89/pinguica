<?php
/**
 * Template Name: Faqs
 */
get_header(); ?>
    <div class="p-faqs">
        <section class="faqs">
            <div class="container-fluid px-0">
                <div class="row">
                    <div class="col-lg-6 bg-gray py-5">
                        <div class="row py-5 my-5">
                            <div class="col-10 col-lg-8 mx-auto py-5 my-5">
                                <div class="text-center mb-5 pb-5">
                                    <h2 class="marker-purple">Preguntas <br>frecuentes</h2>
                                </div>
                                <?php $totalRows = count(get_field('faqs')); ?>
                                <?php if(have_rows('faqs')) : while(have_rows('faqs')) : the_row(); ?>
                                    <div class="faq<?php if(get_row_index() !== $totalRows) : ?> mb-5<?php endif; ?>">
                                        <p class="mb-0">
                                            <strong><span class="marker-purple"><?php echo get_row_index(); ?>.</span><?php the_sub_field('question'); ?></strong> <br><br>
                                            <?php the_sub_field('answer'); ?>
                                        </p>
                                    </div>
                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bg-cover" style="background-image:url(<?php bloginfo('template_url'); ?>/assets/img/img-5.jpg);"></div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>