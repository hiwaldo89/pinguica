<?php
/**
 * Template Name: Service
 */
get_header(); ?>
    <div class="p-services">
        <section class="statement">
            <div class="container-fluid px-0">
                <div class="row">
                    <div class="col-lg-6 bg-gray py-5">
                        <div class="row py-5 my-5">
                            <div class="col-10 col-lg-8 mx-auto text-center py-5 my-5">
                                <h2 class="marker-purple">Servicios</h2>
                                <div class="mb-0 py-5">
                                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                                        <?php the_content(); ?>
                                    <?php endwhile; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bg-cover" style="background-image:url(<?php bloginfo('template_url'); ?>/assets/img/img-2.jpg);"></div>
                </div>
            </div>
        </section>
        <section class="services py-5">
            <div class="container pt-5">
                <div class="row">
                    <?php if(have_rows('services')) : while(have_rows('services')) : the_row(); ?>
                        <div class="col-6 col-lg-4 text-center mb-5 pb-5">
                            <a href="#service-modal-<?php echo get_row_index(); ?>" class="display-service-modal">
                                <div class="service-item">
                                    <?php $serviceImg = get_sub_field('image'); ?>
                                    <div class="service-item__img bg-cover" style="background-image:url(<?php echo $serviceImg['url']; ?>);"></div>
                                </div>
                                <div class="mt-5 px-3">
                                    <h3 class="h4 text-center marker-purple"><?php the_sub_field('title'); ?></h3>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </section>
    </div>

    <?php if(have_rows('services')) : while(have_rows('services')) : the_row(); ?>
        <div id="service-modal-<?php echo get_row_index(); ?>" class="modal fade service-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header position-absolute w-100 border-0" style="z-index: 2;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="container-fluid">
                            <div class="row align-items-center">
                                <div class="col-lg-5 px-lg-0 d-flex">
                                    <div class="service-slider">
                                        <?php $galleryImages = get_sub_field('gallery'); ?>
                                        <?php foreach($galleryImages as $image) :?>
                                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="service-slider__slide">
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="p-5">
                                        <div class="service-modal__description mb-5">
                                            <?php the_sub_field('description'); ?>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <i class="far fa-usd-circle mr-2 color-brown h4 mb-0"></i>
                                            <div class="h5 mb-0 marker-purple">A partir de <span class="service-modal__price">
                                                <?php money_format('%.2n', the_sub_field('price_min')); ?>
                                            </span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>
<?php get_footer(); ?>