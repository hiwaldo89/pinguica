<?php
/**
 * Template Name: Home
 */
get_header(); ?>
    <div class="p-home">
        <section class="welcome-section">
            <?php $welcomeSection = get_field('welcome_section'); ?>
            <div class="home-slider">
                <div class="home-slider__slide" style="background-image: url(<?php echo $welcomeSection['image']['url']; ?>);">
                    <div class="text-center">
                        <h1 class="marker-purple">
                            <?php echo $welcomeSection['text']; ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-section">
            <?php $aboutSection = get_field('about_section'); ?>
            <div class="container-fluid px-0">
                <div class="row mx-0">
                    <div class="col-12 bg-gray">
                        <div class="row py-5">
                            <div class="col-lg-6 mx-lg-auto py-5">
                                <div class="py-5 text-center h3"><?php the_field('welcome_text'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 text-center bg-pink about-section__col1 pt-5">
                        <!-- <div class="py-5">
                            <p class="h4 d-inline-block text-left py-5">
                                <?php echo $aboutSection['column1']; ?>
                            </p>
                        </div> -->
                    </div>
                    <div class="col-lg-6 text-center">
                        <div class="about-section__col2 pt-5">
                            <!-- <p class="h4 d-inline-block text-left py-5">
                                <?php echo $aboutSection['column2']; ?>
                            </p> -->
                            <div class="img-collage">
                                <div class="about-section__img1">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/img-2.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="about-section__img2 paroller" data-paroller-factor="0.3" data-paroller-type="foreground">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/img-3.jpeg" alt="" class="img-fluid">
                                </div>
                                <div class="about-section__img3 paroller" data-paroller-factor="0.2" data-paroller-type="foreground">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/img-4.jpeg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/logo-gray.svg" alt="" class="about-section__stamp">
                    </div>
                </div>
            </div>
            <div class="bg-mint py-5 banner">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/star.svg" alt="" class="star paroller" data-paroller-factor="0.3" data-paroller-type="foreground">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/squares.svg" alt="" class="squares paroller" data-paroller-factor="0.4" data-paroller-type="foreground">
                <div class="container py-5">
                    <div class="row py-5">
                        <div class="col-lg-6 mx-auto py-5">
                            <div class="py-5">
                                <p class="text-center">
                                    <span class="marker">
                                        <?php the_field('banner'); ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="services pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-11">
                        <div class="row align-items-center">
                            <div class="col-lg-6 ml-auto">
                                <div class="services-description bg-gray">
                                    <div class="services-description__inner py-5">
                                        <div class="row py-5">
                                            <div class="col-8 mx-auto text-center py-5">
                                                <?php $servicesBanner = get_field('services_banner'); ?>
                                                <p class="mb-5 h4">
                                                    <?php echo $servicesBanner['text']; ?>
                                                </p>
                                                <a href="<?php echo get_permalink( get_page_by_path( 'servicios' ) ); ?>" class="marker-green h5">Servicios</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="services-img-wrapper">
                                    <div class="box" style="background-image:url(<?php echo $servicesBanner['image']['url']; ?>);"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>